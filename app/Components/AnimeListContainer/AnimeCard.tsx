import React, { useState } from "react";
import Image from "next/image";
import CardVideoBackground from "./CardVideoBackground";
import { Anime } from "@/app/Interfaces/Interfaces";
import { SparklesIcon } from "@heroicons/react/20/solid";
import { useRouter } from 'next/navigation'
import Link from "next/link";
const AnimeCard = (anime: Anime) => {
  // State to track hover status
  const [isHovered, setIsHovered] = useState(false);
  const router = useRouter()

  const handleOnCardClick = () =>{
    router.push(`/anime/${anime.mal_id}`);
  }

  return (
    <div
      className="w-full group h-full bg-slate-100 overflow-hidden rounded-md relative"
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <Image
        height={500}
        width={300}
        quality={100}
        className="w-full h-auto"
        alt="anime-card"
        src={anime.images.webp.large_image_url}
      />
      {/* Conditionally render the video background when hovered */}
      {isHovered && (
        <div className="w-full h-full transition-all absolute top-0 left-0">
          <Link href={`/anime/${anime.mal_id}`} className="w-full group h-full relative p-2 flex flex-col text-center items-center z-20 justify-center">
            <h1 className="text-white font-bold uppercase text-2xl cursor-pointer group-hover:text-primary-100">
              {anime.title}
            </h1>
            <div className="flex flex-row items-center">
              <SparklesIcon className="w-5 h-5 text-white" />
              <h1 className="text-white font-semibold">
                Score: {anime.score} ({anime.scored_by})
              </h1>
            </div>
            <div className="flex flex-row gap-2 flex-wrap">
              {anime.genres.map((genre, index) => (
                <span className="text-white bg-primary-100 px-2 py-1 rounded-full" key={index}>
                  {genre.name}
                </span>
              ))}
            </div>
            <p className="text-white">
              {anime.synopsis
                ? `${anime.synopsis.slice(0, 100)}${
                    anime.synopsis.length > 100 ? "..." : ""
                  }`
                : ""}
            </p>
          </Link>
          <CardVideoBackground frame={anime.trailer.embed_url} />
        </div>
      )}
    </div>
  );
};

export default AnimeCard;
