"use client";
import React, { Suspense, useEffect, useState } from "react";
import AnimeListShowCase from "./AnimeListShowCase";
import { Anime, Recomendation } from "@/app/Interfaces/Interfaces";
import Loading from "../Loading/Loading";
import AnimeCard from "./AnimeCard";
import { getRecentEpisodes } from "@/app/Utils/Utils";
import RecomendationCard from "../Recomendations/RecomendationCard";
const AnimeListContainer = () => {
  const [animeList, setAnimeList] = useState<Anime[] | null>(null);

  useEffect(() =>{
    const getRecent = async() =>{
      const recent = await getRecentEpisodes()
      console.log(recent);
      //setAnimeList(recent)
    }
    getRecent();
  },[])

  const queries = [
    {
      title: "airing",
      query: "",
      status: "airing",
      sfw: false,
      genres: "1",
    },
    {
      title: "Action",
      query: "",
      status: "",
      sfw: false,
      genres: "1",
    },
    {
      title: "Adventure",
      query: "",
      status: "",
      sfw: false,
      genres: "2",
    },
    {
      title: "Fantasy",
      query: "",
      status: "",
      sfw: false,
      genres: "10",
    },
  ];

  return (
    <section id="startw" className="0 w-full h-fit relative z-30">
      <div className="w-full h-fit bg-background-300 flex flex-col gap-6 md:p-5 ">
      <h1 className="text-5xl text-white uppercase font-bold mb-[-24px]">
              Recent Episodes
            </h1>
        <div className="w-full h-[450px] bg-primary-300">
          {animeList != null ? (
            <ul className="w-full h-full flex gap-4 px-2 py-5 overflow-auto">
              {animeList.map((anime, index) => (
                <li className="h-full min-w-[300px]" key={index}>
                  <AnimeCard {...anime} />
                </li>
              ))}
            </ul>
          ) : (
            <Loading />
          )}
        </div>

        {queries.map((query, index) => (
          <div key={index}>
            <h1 className="text-5xl text-white uppercase font-bold">
              {query.title}
            </h1>
            <AnimeListShowCase
              query={query.query}
              status={query.status}
              sfw={query.sfw}
              genres={query.genres}
              type=""
              order=""
            />
          </div>
        ))}
      </div>
    </section>
  );
};

export default AnimeListContainer;
