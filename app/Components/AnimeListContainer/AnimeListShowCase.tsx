import React, { useEffect, useState } from "react";
import { Anime, onQuerySearchProps } from "@/app/Interfaces/Interfaces";
import { onQuerySearch } from "@/app/Utils/Utils";
import AnimeCard from "./AnimeCard";
import Loading from "../Loading/Loading";
const AnimeListShowCase = ({ query, status, sfw, genres }: onQuerySearchProps) => {

    const [animeList, setAnimeList] = useState<Anime[] | null>(null);
    useEffect(() =>{
        const getAnimeList = async () =>{
            const result = await onQuerySearch({query, status, sfw, genres, order:"popularity", type:""})
            console.log(result);
            setAnimeList(result)
        }
        getAnimeList();
    },[query, sfw, status,genres])


  return (
  <div className="w-full h-[450px] bg-primary-300">
    {
    animeList != null ? (
        <ul className="w-full h-full flex gap-4 px-2 py-5 overflow-auto">
            {animeList.map((anime,index) =>(
                <li className="h-full min-w-[300px]" key={index}>
                <AnimeCard {...anime}/>
                </li>
            ))}
        </ul>
    ) : (
        <Loading/>
    )
}
  </div>
  )
};

export default AnimeListShowCase;
