import React, { useEffect, useState } from "react";

const CardVideoBackground = ({ frame }: { frame: string }) => {
  // Initialize the URL with autoplay, mute, minimal branding, and loop parameters
  const [url, setUrl] = useState<string>("");
  useEffect(() => {
    if (frame) {
      // Extract video ID for looping
      const videoIDMatch = frame.match(/(?:youtube\.com.*(?:\\?|&)v=|youtu\.be\/)([^\\?&"'>]+)/);
      const videoID = videoIDMatch ? videoIDMatch[1] : null;

      // Append parameters for autoplay, mute, modest branding, and loop
      // Note: For loop to work, 'playlist' parameter must be set with the video's ID
      const baseFrame = frame.includes('?')
        ? `${frame}&autoplay=1&mute=1&controls=0&modestbranding=1&loop=1`
        : `${frame}?autoplay=1&mute=1&controls=0&modestbranding=1&loop=1`;

      const newUrl = videoID ? `${baseFrame}&playlist=${videoID}` : baseFrame;
      setUrl(newUrl);
    }
  }, [frame]);

  return (
    <div className="w-full h-full absolute top-0">
      <iframe
        src={url}
        frameBorder="0"
        allow="autoplay; fullscreen"
        loading="lazy"
        className="h-full w-full scale-150"
      />
      <div className="absolute inset-0 bg-black opacity-60"></div>
    </div>
  );
};

export default CardVideoBackground;
