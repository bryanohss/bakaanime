import { getAnimeEpisodes } from "@/app/Utils/Utils";
import React, { useEffect, useState } from "react";
import Loading from "../Loading/Loading";
import Image from "next/image";
import { Episode } from "@/app/Interfaces/Interfaces";

const EpisodesContainer = ({ id }: { id: number }) => {
  const [episodes, setEpisodes] = useState<Episode[] | null>(null);

  useEffect(() => {
    const getEpisodes = async (id: number) => {
      const eps = await getAnimeEpisodes(id);
      console.log(eps);
      setEpisodes(eps);
    };
    getEpisodes(id);
  }, [id]);
  if (episodes == null) {
    return <Loading />;
  }
  return (
    <ul className="w-full h-full overflow-y-scroll flex flex-col gap-3 rounded-lg bg-primary-200 p-3">
      {episodes.map((episode, index) => {
        return (
          <li
            className="w-full group flex lg:min-h-[150px] flex-col md:flex-row bg-primary-300 cursor-pointer rounded-md p-2"
            key={index}
          >
            <Image
              width={300}
              height={150}
              className="h-full w-auto rounded-md"
              alt="episode-cover"
              src={episode.images.jpg.image_url}
              quality={100}
            />
            <div className="flex flex-col p-1">
                <h1 className="text-white transition-colors text-2xl group-hover:text-slate-500 font-bold">{episode.episode}</h1>
                <h1 className="text-white transition-colors text-xl group-hover:text-slate-500">{episode.title}</h1>

            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default EpisodesContainer;
