import React from 'react';

const BackgroundVideo = ({ path }: { path: string }) => {
  return (
    <div className='fixed inset-0'>
      <video 
        muted 
        loop 
        autoPlay 
        playsInline 
        className='w-full h-full object-cover'
      >
        <source src={path} type='video/mp4' />
      </video>
      {/* Overlay div with semi-transparent background */}
      <div className="absolute inset-0 bg-black opacity-50"></div>
    </div>
  );
}

export default BackgroundVideo;
