import React from "react";
import Image from "next/image";
import Link from "next/link";
const HeroSection = () => {
  return (
    <div className="w-full h-full grid relative z-10 bg-transparent">
      <div className="flex flex-col place-self-center justify-center items-center text-center">
        <Image
          alt="logo"
          quality={100}
          className="w-[200px] md:w-[300px] h-auto"
          width={500}
          height={500}
          src={"/logo.svg"}
        />
        <h1 className="text-white mt-1 md:text-[80px] text-[40px] uppercase font-bold">
          Bak
          <span className="text-transparent bg-clip-text bg-gradient-to-r from-primary-100 to-primary-200">
            anime
          </span>
        </h1>

        <a href={"#startw"} className="text-primary-100 overflow-hidden group relative w-[200px] uppercase font-bold h-[70px] bg-transparent border-[3px] rounded-sm border-primary-200 text-x">
          <h1 className="relative group-hover:text-white transition-colors z-10 mt-5">Start Watching</h1>
          <div className="w-full h-full absolute transition-transform group-hover:translate-y-full bottom-full bg-primary-200" />
        </a>
      </div>
    </div>
  );
};

export default HeroSection;
