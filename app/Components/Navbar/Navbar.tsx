"use client";
import React, { useState } from "react";
import Link from "next/link";
import Searchbar from "../Searchbar/Searchbar";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";

const Navbar = () => {
  const [navActive, setNavActive] = useState<boolean>(false);
  const NAV_HEIGHT: string = "120px";

  return (
    <div 
    style={{ height: NAV_HEIGHT }}
    className="relative z-30  min-w-screen">
    <nav
      className="w-full h-full relative flex items-center justify-around px-1"
    >
      <Link href={"/"} className="text-3xl md:text-5xl font-bold text-white uppercase">
        <span className="text-transparent bg-clip-text bg-gradient-to-r from-primary-100 to-primary-200">Baka</span>
        nime
      </Link>
      
      <div className="hidden xl:flex gap-6 items-center justify-between">
        <ul className="flex gap-6 mr-7">
          <li>
            <Link href="/" className="text-white text-xl uppercase hover:text-primary-100 transition-colors">
              Home
            </Link>
          </li>
          <li>
            <Link href="/view" className="text-white text-xl uppercase hover:text-primary-100 transition-colors">
              View More
            </Link>
          </li>
          <li>
            <Link href="/secret" className="text-white text-xl uppercase hover:text-primary-100 transition-colors">
              Secret
            </Link>
          </li>
        </ul>
        <Searchbar />
      </div>

      <div className="xl:hidden flex">
        {navActive ? (
          <XMarkIcon
            onClick={() => setNavActive(false)}
            className="w-10 h-10 text-white xl:hidden cursor-pointer"
          />
        ) : (
          <Bars3Icon
            onClick={() => setNavActive(true)}
            className="w-10 h-10 text-white xl:hidden cursor-pointer"
          />
        )}
      </div>
      <ul
        style={{ top: NAV_HEIGHT }}
        className={`xl:hidden flex flex-col fixed right-0 z-40 bg-background-200 transition-transform ease-out duration-300 ${
          navActive ? "translate-x-20" : "translate-x-full"
        } w-fit px-3 py-5 gap-5 z-50 h-fit`}
      >
        <li className="text-white text-xl uppercase">
          <Link href="/">Home</Link>
        </li>
        <li className="text-white text-xl uppercase">
          <Link href="/view">View More</Link>
        </li>
        <li className="text-white text-xl uppercase">
          <Link href="/secret">Secret</Link>
        </li>
        <li>
          <Searchbar />
        </li>
      </ul>
    </nav>
    </div>
  );
};

export default Navbar;
