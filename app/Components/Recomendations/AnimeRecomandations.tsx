import React, { useEffect, useState } from 'react'
import AnimeCard from '../AnimeListContainer/AnimeCard';
import Loading from '../Loading/Loading';
import { Anime, Recomendation } from '@/app/Interfaces/Interfaces';
import { getAnimeRecomendations } from '@/app/Utils/Utils';
import RecomendationCard from './RecomendationCard';

const AnimeRecomandations = ({id}:{id:number}) => {
    const [animeList, setAnimeList] = useState<Recomendation[] | null>(null);
    useEffect(() =>{
        const getAnimeList = async () =>{
            const result = await getAnimeRecomendations(id)
            console.log(result);
            setAnimeList(result)
        }
        getAnimeList();
    },[id])


  return (
  <div className="w-full h-fit">
    {
    animeList != null ? (
        <ul className="w-full h-fit flex gap-4 px-2 py-5 overflow-x-auto">
            {animeList.map((anime,index) =>(
                <li className="h-full min-w-[300px]" key={index}>
                    <RecomendationCard {...anime}/>
                </li>
            ))}
        </ul>
    ) : (
        <Loading/>
    )
}
  </div>
  )
}

export default AnimeRecomandations