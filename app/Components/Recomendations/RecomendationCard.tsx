import React, { useState } from 'react'
import Image from 'next/image';
import Link from 'next/link';
import { SparklesIcon } from '@heroicons/react/24/solid';
import { Recomendation } from '@/app/Interfaces/Interfaces';
const RecomendationCard = (anime:Recomendation) => {
    const [isHovered, setIsHovered] = useState(false);

  return (
    <Link href={`/anime/${anime.entry.mal_id}`}
      className="w-full group h-full group overflow-hidden rounded-md relative"
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <Image
        height={400}
        width={400}
        quality={100}
        className="max-h-[400px] w-auto rounded-md"
        alt="anime-card"
        src={anime.entry.images.jpg.large_image_url}
      />
      
        <div className="w-full h-full transition-all absolute top-0 left-0">
          <div className="w-full h-full relative p-2 flex flex-col text-center items-center z-20 justify-end">
 
          </div>
        </div>
    </Link>
  )
}

export default RecomendationCard