import React, { useState } from "react";
import { MagnifyingGlassIcon } from "@heroicons/react/24/outline";
import { onQuerySearch } from "@/app/Utils/Utils";
import Image from "next/image";
import Link from "next/link";
import { Anime } from "@/app/Interfaces/Interfaces";
const Searchbar = () => {

  const [query, setQuery] = useState<string>("");
  const [animes, setAnimes] = useState<Anime[] | null>(null);

  const backgroundType = {
    tv: "bg-type_tv",
    movie: "bg-type_movie",
    ova: "bg-type_ova",
    special: "bg-type_special",
    tv_special: "bg-tv_special",
  };

  const onSearchSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const animeList = await onQuerySearch({query, status:"", sfw:false, type: "", order: "popularity", genres:""});
    console.log(animeList);
    setAnimes(animeList);
  };

  return (
    <form
      method="GET"
      onSubmit={(e) => onSearchSubmit(e)}
      className="flex w-fit relative"
    >
      <input
        className="w-[60%] xl:w-[350px] h-10 px-3 text-white border-primary-200 border-2 rounded-sm bg-primary-300"
        onChange={(e) => setQuery(e.target.value)}
        type="text"
        name="search"
      />
      {/* Hide the default submit button visually but keep it accessible */}
      <input
        type="submit"
        value="Submit"
        className="sr-only"
        aria-hidden="true"
      />
      <button
        type="submit"
        className="flex justify-center items-center w-10 h-10"
      >
        <MagnifyingGlassIcon
          style={{ strokeWidth: "3px" }}
          className="w-5 h-5 text-white transition-colors hover:text-primary-100"
        />
      </button>
      {animes && (
        <ul
        className="flex flex-col absolute w-[215px] h-[200px] p-1 overflow-y-scroll top-full mt-1 bg-primary-300 rounded-sm">
          {animes.map((anime, index) => (
            <Link
              href={`/anime/${anime.mal_id}`}
              key={index}
              className="w-full group flex rounded-md mb-2 min-h-[90px] bg-primary-200 p-1"
            >
              <Image
                height={300}
                width={300}
                className="w-[90px] h-auto rounded-sm"
                alt="anime_pic"
                src={anime.images.webp.large_image_url}
              />
              <div className="px-1 flex flex-col">
                <h1 className="text-white group-hover:text-primary-100">{anime.title_english}</h1>
                <div className={`text-white w-fit px-1 rounded-sm bg-type_tv`}>
                  {anime.type}
                </div>
              </div>
            </Link>
          ))}
        </ul>
      )}
    </form>
  );
};

export default Searchbar;
