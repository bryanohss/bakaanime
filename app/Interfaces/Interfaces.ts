export interface Anime {
  images: {
    webp: {
      large_image_url: string;
    };
  };
  mal_id: number;
  title_english: string;
  title:string;
  type: string;
  trailer:{
    embed_url:string;
  }
  episodes:number
  score: number;
  scored_by: number;
  genres:[
    {
        mal_id:number;
        name:number;
    }
  ]
  background:string;
  title_japanese: string;
  synopsis: string;
  airing:boolean;
  status:string;

}

export interface onQuerySearchProps {
  query: string;
  status: string;
  sfw: boolean;
  genres:string;
  type:string
  order:string
}

export interface Episode{
  episode: string;
  images:{
    jpg:{
      image_url:string;
    }
  }
  title:string
}

export interface Recomendation{
  entry:{
    images:{
      jpg:{
        large_image_url:string
      }
    }
    title:string;
    mal_id:number

  }
}

export interface Genre{
  mal_id:number;
  name:string;
}
