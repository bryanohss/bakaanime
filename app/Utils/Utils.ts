import axios from "axios";
import rateLimit from "axios-rate-limit";
import NodeCache from "node-cache";
import { onQuerySearchProps, Episode } from "../Interfaces/Interfaces";

// Set up axios with rate limiting
const http = rateLimit(axios.create(), {
  maxRequests: 1,
  perMilliseconds: 1000,
  maxRPS: 1,
});

// Simple in-memory cache setup
const cache = new NodeCache({ stdTTL: 100, checkperiod: 120 });

export const onQuerySearch = async ({
  query,
  status,
  sfw,
  genres,
  type,
  order
}: onQuerySearchProps): Promise<any> => {
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";

  // Use the query parameters to create a unique cache key
  const cacheKey = `search:${query}:${status}:${sfw}:${genres}:${type}:${order}`;
  // Try to fetch from cache first
  const cachedResult = cache.get(cacheKey);
  if (cachedResult) {
    return cachedResult;
  }

  let params = `page=1&q=${encodeURIComponent(query)}&sfw=${sfw}`;
  // Append status if it's provided
  if (status) params += `&status=${encodeURIComponent(status)}`;
  // Append genres directly to avoid URL encoding of commas
  if (genres) params += `&genres=${genres}`; // Directly appending genres
  if (type) params += `&type=${type}`
  if (order) params += `&order_by=${order}`
  console.log(params)
  try {
    const response = await http.get(`${API_URL}/anime?${params}`);
    const data = response.data.data;
    // Cache the result before returning
    cache.set(cacheKey, data);
    return data;
  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }
};

export const onGetAnimeByID = async (id: string): Promise<any> => {
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";

  const cacheKey = `anime:${id}`;
  const cachedResult = cache.get(cacheKey);
  if (cachedResult) {
    return cachedResult;
  }

  try {
    const response = await http.get(`${API_URL}/anime/${id}/full`);
    const images = await http.get(`${API_URL}/anime/${id}/pictures`);

    const data = response.data.data;
    const imageData = images.data.data;

    //cache.set(cacheKey, { data: data, images: imageData });

    return [data, imageData];
  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }
};

export const getAnimeEpisodes = async (id: number): Promise<any> => {
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";
  const cacheKey = `episodes:${id}`;
  const cachedResult = cache.get(cacheKey);
  if (cachedResult) {
    return cachedResult;
  }

  try {
    const response = await http.get(`${API_URL}/anime/${id}/videos/episodes`);
    cache.set(cacheKey, response.data.data); // Serialize data before caching
    return response.data.data;

  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }
}

export const getAnimeRecomendations = async(id:number):Promise<any> =>{
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";
  const cacheKey = `recomendations:${id}`;
  const cachedResult = cache.get(cacheKey);
  if (cachedResult) {
    return cachedResult;
  }

  try {
    const response = await http.get(`${API_URL}/anime/${id}/recommendations`);
    cache.set(cacheKey, response.data.data);
    return response.data.data;

  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }
}

export const getRecentEpisodes = async ():Promise<any> =>{
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";
  const cacheKey = `recent`;
  const cachedResult = cache.get(cacheKey);
  if (cachedResult) {
    return cachedResult;
  }

  try {
    const response = await http.get(`${API_URL}/schedules?&kids=false&sfw=false&limit=20`);
    cache.set(cacheKey, response.data.data);
    return response.data.data;

  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }
}

export const getAnimeGenres = async ():Promise<any> =>{
  const API_URL = process.env.API_URL || "https://api.jikan.moe/v4";
  const cacheKey = `genres`;
  

  try {
    const response = await http.get(`${API_URL}/genres/anime`);
    //cache.set(cacheKey, response);
    return response.data.data;

  } catch (error) {
    console.error("Failed to fetch data from API", error);
    throw new Error("Failed to fetch data from API");
  }


}
