"use client";
import { Anime } from "@/app/Interfaces/Interfaces";
import React, { useEffect, useState } from "react";
import { onGetAnimeByID } from "@/app/Utils/Utils";
import Loading from "@/app/Components/Loading/Loading";
import CardVideoBackground from "@/app/Components/AnimeListContainer/CardVideoBackground";
import Image from "next/image";
import EpisodesContainer from "@/app/Components/EpisodesContainer/EpisodesContainer";
import AnimeRecomandations from "@/app/Components/Recomendations/AnimeRecomandations";

const AnimePage = ({ params }: { params: { id: string } }) => {
  const [anime, setAnime] = useState<Anime | null>(null);
  const [animePics, setAnimePic] = useState<[] | null>(null);
  const [selectedPic, setSelected] = useState<any | null>(null);

  const COL_WIDTH = "350px";
  useEffect(() => {
    const fetchAnime = async () => {
      if (params.id == null || anime != null || selectedPic != null) {
        return;
      }
      const data = await onGetAnimeByID(params.id);
      setAnime(data[0]);
      setAnimePic(data[1]);
    };
    fetchAnime();
  }, [params.id, anime, selectedPic]);

  useEffect(() => {
    if (animePics != null && animePics.length > 0) {
      const index = Math.floor(Math.random() * animePics.length);
      setSelected(animePics[index]);
    }
  }, [animePics]);

  if (!anime || !selectedPic) {
    return <Loading />;
  }

  return (
    <main className="w-screen h-screen flex items-center flex-col">

      <section className="w-full h-[500px] hidden absolute top-0 left-0 overflow-hidden">
        <CardVideoBackground frame={anime.trailer.embed_url} />
      </section>

      {/*DESKTOP VIEW */}
      <div className="w-[70%] hidden xl:flex h-fit place-self-center mt-5 flex-col  z-10 relative">
        <div className="w-full h-fit flex">
          <div className="w-fit h-fit bg-primary-200 rounded-lg p-2">
            <Image
              quality={100}
              width={1000}
              height={1000}
              alt="anime_cover"
              style={{ width: COL_WIDTH }}
              className="h-auto rounded-md"
              src={selectedPic.jpg.large_image_url}
            />
          </div>
          <div
            style={{ width: `calc(100% - ${COL_WIDTH})` }}
            className="h-full "
          >
            <div className="w-full h-1/2 flex flex-col justify-end p-3">
              <div className="flex gap-2">
                <h1 className="text-5xl text-white font-bold ">
                  {anime.title}
                </h1>
                <h1 className="px-2 py-1 text-xl flex items-center font-bold rounded-md bg-primary-200 text-white uppercase">
                  {anime.type}
                </h1>
              </div>
              <div className="flex gap-2 mt-3">
                <span className="text-white px-2 py-1 bg-primary-200 rounded-md ">
                  {anime.title_english}
                </span>
                <span className="text-white px-2 py-1 bg-primary-200 rounded-md">
                  {anime.title_japanese}
                </span>
              </div>
            </div>
            <div className="w-full h-1/2 p-3">
              <h1 className="text-white uppercase font-bold text-2xl">
                Background
              </h1>
              <div className="flex gap-2">
                {anime.genres.map((genre, index) => (
                  <span
                    key={index}
                    className="text-white text-sm px-2 py-1 bg-primary-200 rounded-md"
                  >
                    {genre.name}
                  </span>
                ))}
              </div>
              <p className="text-white">
                {anime.synopsis
                  ? `${anime.synopsis.slice(0, 400)}${
                      anime.synopsis.length > 400 ? "..." : ""
                    }`
                  : ""}
              </p>
            </div>
          </div>
        </div>
        {/*Episodes Container */}

        <div className="w-full h-[700px]  p-2 flex">
          <div style={{ width: COL_WIDTH }} className="h-[500px]">
            <div className="w-full h-[60px] bg-primary-200 rounded-md flex items-center justify-center">
              <span className="text-white font-bold text-3xl uppercase">
                {anime.status}
              </span>
            </div>
          </div>
          <div
            style={{ width: `calc(100% - ${COL_WIDTH})` }}
            className="p-3 flex flex-col"
          >
            <h1 className="text-white text-4xl uppercase font-bold">
              EPISODES
            </h1>
            <EpisodesContainer id={anime.mal_id} />
          </div>
        </div>

        {/*Recomendations Container */}
        <div className="w-full h-fit  mb-[50px] p-4">
          <div className="w-full h-fit bg-primary-200 rounded-md p-2">
            <h1 className="text-white font-bold text-3xl">
              If you like {anime.title}
              <br /> you may want to check out:
            </h1>
            <AnimeRecomandations id={anime.mal_id} />
          </div>
        </div>
      </div>


      {/*MOVLIE VIEW */}
      <div className="w-[90%] xl:hidden flex h-fit place-self-center mt-5 flex-col items-center z-10 relative">
        <div className="w-fit h-fit bg-primary-200 place-self-center rounded-lg p-2">
            <Image
              quality={100}
              width={1000}
              height={1000}
              alt="anime_cover"
              style={{ width: COL_WIDTH }}
              className="h-auto rounded-md"
              src={selectedPic.jpg.large_image_url}
            />
          </div>  
          <div className="flex gap-2 my-5">
            <h1 className="text-white text-2xl font-bold">{anime.title}</h1>
            <h1 className="px-2 py-1 text-lg flex items-center font-bold rounded-md bg-primary-200 text-white uppercase">
                    {anime.type}
                  </h1>
          </div>
          <div className="flex gap-2 flex-wrap">
                {anime.genres.map((genre, index) => (
                  <span
                    key={index}
                    className="text-white text-sm px-2 py-1 bg-primary-200 rounded-md"
                  >
                    {genre.name}
                  </span>
                ))}
              </div>
              <div className="w-full h-1/2 mt-4">
              <h1 className="text-white uppercase font-bold text-2xl">
                Background
              </h1>
            
              <p className="text-white">
                {anime.synopsis
                  ? `${anime.synopsis.slice(0, 300)}${
                      anime.synopsis.length > 300 ? "..." : ""
                    }`
                  : ""}
              </p>
            </div>
            <div
            className="p-3 flex flex-col w-full"
          >
            <h1 className="text-white text-4xl uppercase font-bold">
              EPISODES
            </h1>
            <EpisodesContainer id={anime.mal_id} />
          </div>
          
            
      </div>
 
    </main>
  );
};

export default AnimePage;
