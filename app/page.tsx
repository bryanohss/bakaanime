import Image from "next/image";
import HeroSection from "./Components/HeroSection/HeroSection";
import BackgroundVideo from "./Components/HeroSection/BackgroundVideo";
import AnimeListContainer from "./Components/AnimeListContainer/AnimeListContainer";
export default function Home() {
  const NAV_HEIGHT = "120px";

  return (
    <main className="w-screen h-fit flex flex-col">
      <div
        style={{ height: `calc(100vh - ${NAV_HEIGHT})` }}
        className="w-full"
      >
        <HeroSection />
        <BackgroundVideo path="/background_video.mp4" />
      </div>
      
      <AnimeListContainer/>
    </main>
  );
}
