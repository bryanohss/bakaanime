import Navbar from "./Components/Navbar/Navbar";

export default function Template({ children }: { children: React.ReactNode }) {
    return (
        <>
        <Navbar/>
            {children}
        </>
    )
  }