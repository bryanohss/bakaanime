"use client";
import React, { useEffect, useState } from 'react'
import { ArrowDownIcon } from '@heroicons/react/24/outline';
import { getAnimeGenres, onQuerySearch } from '../Utils/Utils';
import { Anime, Genre } from '../Interfaces/Interfaces';
import AnimeCard from '../Components/AnimeListContainer/AnimeCard';
import Loading from '../Components/Loading/Loading';

const ViewMorePage = () => {
  const NAV_HEIGHT = "120px";

  const [genreList, setGenreList] = useState<Genre[]|null>(null);
  const [selectedWindow, setSelected] = useState<string>("");
  const [selectedGenres, setSelectedGenres] = useState<number[]>([]);
  const [selectedType, setSelectedType] = useState<string>("");
  const [selectedStatus, setSelectedStatus] = useState<string>("");
  const [selectedOrder, setSelectedOrder] = useState<string>("");
  const [animeList, setAnimeList] = useState<Anime[]|null>(null);
  const [sugestionsGenre, setSuggestions] = useState<Genre[]|null>(null);
  const [selectedGenreNames, setSelectedNames] = useState<string[]|null>(null);

  const getAnimeResult = async (e?: React.FormEvent<HTMLFormElement>) =>{
    e?.preventDefault();
    let genreQuery = "";
    if(genreList && genreList.length > 0){
        genreQuery = selectedGenres.join(",");
        console.log(genreQuery)
    }
    console.log(selectedType)
    const result = await onQuerySearch({query: "", status: selectedStatus, sfw: false, order:selectedOrder, genres:genreQuery, type: selectedType});
    console.log(result)
    setAnimeList(result);

 }

 const handleGenreChange = (name: string, genreId: number, isChecked: boolean): void => {
    if (isChecked) {
      setSelectedNames((prev) => [...new Set([...(prev ?? []), name])]);
      setSelectedGenres((prevSelectedGenres) => [...new Set([...(prevSelectedGenres ?? []), genreId])]);
    } else {
      setSelectedNames((prev) =>
        prev ? prev.filter((existing) => existing !== name) : []
      );
  
      setSelectedGenres((prevSelectedGenres) =>
        prevSelectedGenres ? prevSelectedGenres.filter((id) => id !== genreId) : []
      );
    }
  };
  

  useEffect(() =>{
    const getGenres = async () =>{
        const result = await getAnimeGenres();
        setGenreList(result)
    }
    getGenres();
    getAnimeResult();
  },[])

  const handeWindow = (window:string) =>{
    if(window == "genre"){
        if(selectedWindow == "genre"){
            setSelected("");
        } else{
            setSelected(window);

        } 
    }
    if(window == "type"){
        if(selectedWindow == "type"){
            setSelected("");
        } else{
            setSelected(window);

        } 
    }
    if(window == "state"){
        if(selectedWindow == "state"){
            setSelected("");
        } else{
            setSelected(window);

        } 
    }
    if(window == "order"){
        if(selectedWindow == "order"){
            setSelected("");
        } else{
            setSelected(window);

        } 
    }
  }

  const handleGenreSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    if (genreList) {
      const searchValue = e.target.value;
      // Create a RegExp object, setting the 'i' flag for case-insensitive search.
      const regex = new RegExp(searchValue, 'i');
  
      const suggestions = genreList.filter((genre) => regex.test(genre.name));
      setSuggestions(suggestions);
    }
  };
  

  return (
    <main 
    className="w-screen h-fit flex flex-col items-center p-3">
        <div className='w-[80%] md:w-[90%]  flex flex-col  p-2'>
            <h1 className='text-3xl mb-2 text-center text-white font-bold'>Anime List</h1>
            <form onSubmit={(e) =>{getAnimeResult(e)}} className='w-fit mb-5 place-self-center items-center justify-center flex flex-row h-fit gap-1 md:gap-2 lg:gap-5 xl:gap-14 flex-wrap relative z-10' action="" method="get">

                <div className='w-[100px] h-[50px] flex relative '>
                    <div onClick={() => handeWindow("genre")}  className='cursor-pointer w-full h-full flex items-center justify-center bg-primary-200 rounded-md'>
                        <h1 className='text-white text-center text-xl'>Genre</h1>
                        <ArrowDownIcon style={{strokeWidth: 2}} className='w-5 h-5 text-white'/>
                    </div>
               

                    <div className={`md:min-w-[300px] w-[250px]  z-10  max-w-[300px] h-fit absolute top-full left-0 translate-y-2 ${selectedWindow == "genre" ? "flex": "hidden"}`}>
                        <div className='relative  w-full h-full bg-primary-200 rounded-md grid p-2'>
                            <div className='w-5 h-5 absolute bg-primary-200 rotate-45 -translate-y-[10px] translate-x-2'/>
                            <input onChange={(e) => handleGenreSearch(e)} placeholder='Seach of a genre' className='w-full text-white px-2 h-[40px] bg-primary-300' type="text" name="filter-genre" id="filter-genre" />
                            {selectedGenreNames ? (
                                <div className='w-full h-fit p-2'>
                                    <h1 className='text-white '>Selected:</h1>
                                    <div className='flex gap-2'>
                                    {selectedGenreNames.map((name,index) =>
                                        <span className='text-white underline' key={index}>{name}</span>
                                    )}
                                    </div>
                                </div>

                            ) : (<></>)
                            }
                            <div className='w-full h-fit p-2 max-h-[200px] overflow-y-auto mt-3'>
                                {sugestionsGenre && sugestionsGenre.map((genre, index) => (
                                    <div className='text-white flex gap-1 w-fit h-fit'  key={index}>
                                        <input checked={selectedGenreNames?.includes(genre.name)} className="hidden peer" onChange={(e) => handleGenreChange(genre.name, genre.mal_id, e.target.checked)} type="checkbox" name={genre.name} id={genre.name}/>
                                        <label htmlFor={genre.name} className="block w-5 h-5 bg-gray-300 rounded peer-checked:bg-primary-100 cursor-pointer"></label>
                                        <label htmlFor={genre.name}>{genre.name}</label>
                                    </div>
                                ))}
                            </div>
                            {/*
                                genreList && genreList.map((genre, index) =>(
                                    <div className='text-white flex gap-1 w-fit h-fit'  key={index}>
                                        <input className="hidden peer" onChange={(e) => handleGenreChange(genre.mal_id, e.target.checked)} type="checkbox" name={genre.name} id={genre.name}/>
                                        <label htmlFor={genre.name} className="block w-5 h-5 bg-gray-300 rounded peer-checked:bg-primary-100 cursor-pointer"></label>
                                        <label htmlFor={genre.name}>{genre.name}</label>
                                    </div>
                                ))
                            */}

                        </div>
                    </div>
                </div>
                
                <div className='w-[100px] h-[50px] bg-primary-200  z-10  flex relative justify-center rounded-md items-center'>
                    <div onClick={() => handeWindow("type")}  className='cursor-pointer w-full h-full flex items-center justify-center bg-primary-200 rounded-md'>
                        <h1 className='text-white text-center text-xl'>Type</h1>
                        <ArrowDownIcon style={{strokeWidth: 2}} className='w-5 h-5 text-white'/>
                    </div>

                    <div className={`md:min-w-[400px] w-fit grid-cols-1  max-w-[400px] h-fit absolute top-full left-0 translate-y-2 ${selectedWindow == "type" ? "flex": "hidden"}`}>
                        <div className='relative w-full h-full bg-primary-300 rounded-md grid grid-cols-1  md:grid-cols-4 gap-2 p-2'>
                            <div className='w-5 h-5 absolute bg-primary-300 rotate-45 -translate-y-[10px] translate-x-2'/>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="type" id="tv" onClick={() => setSelectedType("tv")} />
                                <label htmlFor="tv" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="tv" className="cursor-pointer">tv</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="type" id="movie" onClick={() => setSelectedType("movie")}/>
                                <label htmlFor="movie" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="movie" className="cursor-pointer">movie</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="type" id="ova" onClick={() => setSelectedType("ova")}/>
                                <label htmlFor="ova" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="ova" className="cursor-pointer">ova</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="type" id="special" onClick={() => setSelectedType("special")}/>
                                <label htmlFor="special" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="special" className="cursor-pointer">special</label>
                            </div>

                            

                        </div>

                    </div>
                </div>

                <div className='w-[100px] h-[50px] bg-primary-200 flex relative justify-center rounded-md items-center'>
                    <div onClick={() => handeWindow("state")}  className='cursor-pointer w-full h-full flex items-center justify-center bg-primary-200 rounded-md'>
                        <h1 className='text-white text-center text-xl'>State</h1>
                        <ArrowDownIcon style={{strokeWidth: 2}} className='w-5 h-5 text-white'/>
                    </div>
                    <div className={`md:min-w-[400px] w-[200px] grid-cols-1 max-w-[400px] h-fit absolute top-full left-0 translate-y-2 ${selectedWindow == "state" ? "flex": "hidden"}`}>
                        <div className='relative w-full h-full bg-primary-300 rounded-md grid grid-cols-1 md:grid-cols-3 p-2'>
                            <div className='w-5 h-5 absolute bg-primary-300 rotate-45 -translate-y-[10px] translate-x-2'/>

                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="state" id="airing" onClick={() => setSelectedStatus("airing")} />
                                <label htmlFor="airing" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="airing" className="cursor-pointer">airing</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="state" id="complete" onClick={() => setSelectedStatus("complete")}/>
                                <label htmlFor="complete" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="complete" className="cursor-pointer">complete</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="state" id="upcomming" onClick={() => setSelectedStatus("upcoming")}/>
                                <label htmlFor="upcomming" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="upcomming" className="cursor-pointer">upcomming</label>
                            </div>

                        </div>

                    </div>
                </div>

                <div className='w-[100px] h-[50px] bg-primary-200 flex relative justify-center rounded-md items-center'>
                    <div onClick={() => handeWindow("order")}  className='cursor-pointer w-full h-full flex items-center justify-center bg-primary-200 rounded-md'>
                        <h1 className='text-white text-center text-xl'>Order</h1>
                        <ArrowDownIcon style={{strokeWidth: 2}} className='w-5 h-5 text-white'/>
                    </div>
                    <div className={`md:min-w-[300px] w-[150px] gap-2 grid-cols-1 max-w-[400px] h-fit absolute top-full left-0 translate-y-2 ${selectedWindow == "order" ? "flex": "hidden"}`}>
                        <div className='relative w-full h-full bg-primary-300 rounded-md grid grid-cols-1 md:grid-cols-2 p-2'>
                            <div className='w-5 h-5 absolute bg-primary-300 rotate-45 -translate-y-[10px] translate-x-2'/>
                            
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="order" id="title" onClick={() => setSelectedOrder("title")} />
                                <label htmlFor="title" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="title" className="cursor-pointer">title</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="order" id="episodes" onClick={() => setSelectedOrder("episodes")}/>
                                <label htmlFor="episodes" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="episodes" className="cursor-pointer">episodes</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="order" id="score" onClick={() => setSelectedOrder("score")}/>
                                <label htmlFor="score" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-primary-100 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-primary-100"></div>
                                </label>
                                <label htmlFor="score" className="cursor-pointer">score</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="order" id="rank" onClick={() => setSelectedOrder("rank")}/>
                                <label htmlFor="rank" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-blue-600 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-blue-800"></div>
                                </label>
                                <label htmlFor="rank" className="cursor-pointer">rank</label>
                            </div>
                            <div className='text-white flex gap-1 w-fit h-fit items-center'>
                                <input className="hidden peer" type="radio" name="order" id="popularity" onClick={() => setSelectedOrder("popularity")}/>
                                <label htmlFor="popularity" className=" w-5 h-5 bg-gray-300 rounded-full peer-checked:bg-blue-600 cursor-pointer flex items-center justify-center">
                                    <div className="w-3 h-3 bg-white rounded-full peer-checked:bg-blue-800"></div>
                                </label>
                                <label htmlFor="popularity" className="cursor-pointer">popularity</label>
                            </div>

                        </div>

                    </div>
                </div>

                    <input className='text-white hover:bg-primary-300 transition-colors cursor-pointer text-center text-xl w-[100px] h-[50px] bg-primary-200 flex justify-center rounded-md items-center' type="submit" value="Filter" />

            </form>
            {animeList ? (
                <ul className='w-full h-fit grid grid-cols-1 md:grid-cols-2 md:h-[400px] lg:grid-cols-3 xl:grid-cols-4 gap-2 p-2 relative'>
                    {animeList.map((anime,index) =>(
                        <li className="h-fit  max-w-[300px] p-2 bg-primary-300 rounded-md" key={index}>
                        <AnimeCard {...anime}/>
                        </li>

                    ))}
                </ul>

            ) : ( <Loading/>)}
        </div>
      
    
  </main>

  )
}

export default ViewMorePage