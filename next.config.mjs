/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
      remotePatterns: [
        {
          protocol: 'https',
          hostname: 'cdn.myanimelist.net',
          port: '', 
          pathname: '/**', // Matches any image under this path
        },
        {
          protocol: 'https',
          hostname: 'img1.ak.crunchyroll.com',
          port: '', 
          pathname: '/**', // Matches any image under this path
        },

      ],
    },
  };
  
  export default nextConfig;