import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        type_tv: '#392F5A',
        type_movie: '#392F5A',
        type_ova: '#392F5A',
        type_special: '#392F5A',
        type_tv_special: '#392F5A',

        background: {
          100: '#1F1F1F',
          200: '#131313',
          300: '#0E0E0E',
        },
        primary: {
          100: '#E42565',
          200: "#B6174B",
          300: "#91123C",

        }


      }
    },
  },
  plugins: [],
};
export default config;
